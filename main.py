import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = 0#driver.find_element_by_id('th').text
    ah = 0#driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-27 09:32:01.530102
#2018-02-27 09:55:02.618724
#2018-02-27 10:11:01.493865
#2018-02-27 10:31:02.137461
#2018-02-27 10:56:02.153904
#2018-02-27 11:22:01.653018
#2018-02-27 11:44:02.103087
#2018-02-27 12:06:02.086425
#2018-02-27 12:28:01.601521
#2018-02-27 12:55:01.826797
#2018-02-27 13:27:01.974229
#2018-02-27 14:09:01.311891
#2018-02-27 14:22:01.662452
#2018-02-27 14:46:01.346126
#2018-02-27 15:11:01.311441
#2018-02-27 15:30:01.317307
#2018-02-27 15:54:02.159115
#2018-02-27 16:23:01.497488
#2018-02-27 17:34:01.392236
#2018-02-27 18:19:01.284761
#2018-02-27 19:17:01.485391
#2018-02-27 20:32:01.696971